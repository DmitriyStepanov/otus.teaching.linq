﻿using System;
using System.Text;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            var login = string.Empty;

            var pass = string.Empty;

            var command = 0;

        Login:;
            
            //Просим мользователя авторизоваться
            while (!atmManager.Session.IsSessinStarded)
            {
                System.Console.Clear();

                System.Console.WriteLine("Введите логин");
                login = System.Console.ReadLine();
                
                System.Console.WriteLine("Введите пароль");
                pass =  System.Console.ReadLine();

                System.Console.WriteLine(atmManager.LogIn(login, pass));
            }

            //Просим выбрать команду от 1 до 5
            while (command == 0 && command < 6)
            {
                System.Console.Clear();
                    
                System.Console.WriteLine($"Выбрать команду: {Environment.NewLine}" + $"1: Показать информацию об аккаунте {Environment.NewLine}"
                    + $"2: Показать данные о всех счетах {Environment.NewLine}"
                    + $"3: Показать информацию о счетах с истоией операций {Environment.NewLine}"
                    + $"4: Сменить пользователя {Environment.NewLine}"
                    + $"5: Перейти в административный режим {Environment.NewLine}");

                var key = System.Console.ReadKey().KeyChar;

                Int32.TryParse(key.ToString(), out command);
            }
            System.Console.WriteLine();
                
            //Дальше диапозон команд будет расширен. Крутимся в бесконечном цикле пока клиент выбирается соответствующие команды
            while(command > 0 && command < 8)
            {
                if (command == 1)
                {
                    System.Console.WriteLine(atmManager.ShowUsersData());
                }
                else if (command == 2)
                {
                    System.Console.WriteLine(atmManager.ShowUsersAccountsData());
                }
                else if (command == 3)
                {
                    System.Console.WriteLine(atmManager.ShowUsersAccountsDataWhithHistory());
                }
                else if (command == 4)
                {
                    System.Console.Clear();
                    atmManager.LogOut();
                    command = 0;
                    goto Login;
                }
                else if (command == 5)
                {
                    System.Console.Clear();

                    System.Console.WriteLine($"Выбрать команду: {Environment.NewLine}"
                            + $"6: Показать информацию о всех операция пополнения счета {Environment.NewLine}"
                            + $"7: Показать всех пользователей, остатки на счетах которых больше заданной суммы {Environment.NewLine}");

                    var key = System.Console.ReadKey().KeyChar;
                    Int32.TryParse(key.ToString(), out command);

                    //Команды уровня администратора
                    while (command < 6 && command > 8)
                    {
                        key = System.Console.ReadKey().KeyChar; 
                        Int32.TryParse(key.ToString(), out command);
                    }

                    System.Console.WriteLine();

                    atmManager.StartAdminMode();

                    if (command == 6)
                    {
                        System.Console.WriteLine(atmManager.GetInputsOperations());
                    }
                    else if (command == 7)
                    {
                        decimal cashAll = 0;

                        while (cashAll == 0)
                        {
                            System.Console.WriteLine("Введите минимальную сумму остатка на счетах для выборки");

                            Decimal.TryParse(System.Console.ReadLine(), out cashAll);
                        }

                        System.Console.WriteLine(atmManager.GetUsersCashByCashAll(cashAll));
                    }
                }

                System.Console.WriteLine("Выберите команду от 1 до 5 или любое значение, что бы завершить работы");

                if (!Int32.TryParse(System.Console.ReadLine(), out command))
                    command = 0;
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();
                
            return new ATMManager(accounts, users, history);
        }
    }
}