﻿using System;
using System.Collections.Generic;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.ModelsDTO
{
    public class Session
    {
        public UserDTO User { get; private set; }

        public bool IsSessinStarded { get; private set; }

        public Session()
        { }
            
        public static Session SessionStart(UserDTO user)
        {
            return new Session
            {
                User = user,

                IsSessinStarded = true
            };
        }

        public void SessionEnd()
        {
            this.User = null;

            this.IsSessinStarded = false;
        }
    }

    public class UserDTO
    {
        public List<Account> Accounts { get; set; }

        public User UserBase { get; set; } 

        public UserDTO(User user)
        {
            this.UserBase = user;
            this.Accounts = new List<Account>();
        }
    }
}
