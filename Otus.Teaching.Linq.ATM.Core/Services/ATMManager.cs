﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.ModelsDTO;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public Session Session { get; private set; }

        public bool IsAdminMode { get; set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;

            this.Session = new Session();
        }

        //TODO: Добавить методы получения данных для банкомата
        public string LogIn(string login, string password)
        {
            var user = this.Users.FirstOrDefault(u => u.Login == login && u.Password == password);

            if (user == null)
                return "Такая пара значений для логина и пароля не существует";

            var userDto = new UserDTO(user);

            userDto.Accounts = this.Accounts.Where(a => a.UserId == user.Id).ToList();

            this.Session = Session.SessionStart(userDto);
            
            return "Добро пожаловать в Терминал";
        }

        public string LogOut()
        {
            this.Session.SessionEnd();

            return "Ваша сессия Завершена. Спасибо, что воспользовались Терминалом";
        }

        public void StartAdminMode()
        {
            this.IsAdminMode = true;
        }

        public string ShowUsersData()
        {
            return $"SurName: {this.Session.User.UserBase.SurName,5}; FirstName: {this.Session.User.UserBase.FirstName}; " + 
                $"Total Accouns: { this.Session.User.Accounts.Count()}; TotalCash: {this.Session.User.Accounts.Sum(c=>c.CashAll)}";
        }

        public string ShowUsersAccountsData()
        {
            var result = this.CheckUsersAccount();

            result.AppendLine();

            foreach(var acc in this.Session.User.Accounts)
            {
                var line = $"{string.Empty, -10}OpeningDate: {acc.OpeningDate, 5} CachAll: {acc.CashAll, 5}";

                result.AppendLine(line);
            }

            return result.ToString();
        }

        public string ShowUsersAccountsDataWhithHistory()
        {
            var result = this.CheckUsersAccount();
            result.AppendLine();
            foreach (var acc in this.Session.User.Accounts)
            {
                var line = $"{string.Empty,-10}OpeningDate: {acc.OpeningDate,5} CachAll: {acc.CashAll,5}";
                
                result.AppendLine(line);

                result.AppendLine($"{string.Empty,5} История операций: ");

                foreach(var h in this.History.Where(h=>h.AccountId == acc.Id).OrderBy(h=>h.OperationDate).ToList())
                {
                    result.AppendLine($"{string.Empty,-15}OperationDate: {h.OperationDate,5} CashSum: {h.CashSum, 5} OperationType: {h.OperationType,5}");
                }
                
                result.AppendLine();
            }

            return result.ToString();
        }

        public string GetInputsOperations()
        {
            if (!this.IsAdminMode)
                return "Вы не перешли в административный режим";
            
            var result = new StringBuilder("Демонстрация всех операций пополнения счета");
            result.AppendLine();
            var data = from his in this.History.Where(h => h.OperationType == OperationType.InputCash)
                       join acc in this.Accounts on his.AccountId equals acc.Id
                       join user in this.Users on acc.UserId equals user.Id
                       select new {OperDate = his.OperationDate, CashSum = his.CashSum, User = $"SureName: {user.SurName} Name: {user.FirstName}" };

            foreach(var d in data)
            {
                result.AppendLine($"{string.Empty,-10}OperDate: {d.OperDate} CashSum: {d.CashSum} Owner: {d.User}");
            }

            return result.ToString();
        }

        public string GetUsersCashByCashAll(decimal cashAll)
        {
            if (!this.IsAdminMode)
                return "Вы не перешли в административный режим";

            var result = new StringBuilder($"Демонстрация пользователей, остатки на счетах у которых больше: {cashAll}");
            
            result.AppendLine();
            
            var data = from acc in this.Accounts
                       where acc.CashAll > cashAll
                       join u in this.Users on acc.UserId equals u.Id
                       select new { User = $"SureName: {u.SurName} Name:{u.FirstName}", CashAll = acc.CashAll};

            foreach (var d in data)
            {
                result.AppendLine($"{string.Empty,-10}User: {d.User} CashAllSum: {d.CashAll}");
            }

            return result.ToString();


        }
        
        //Helpers
        private StringBuilder CheckUsersAccount()
        {
            var data = string.Empty;

            if (!this.Session.IsSessinStarded)
            {
                data = "Ошибка, сессия не инициирована";
            }
            else if(this.Session.User.Accounts.Count() == 0)
            {
                data = "У Вас пока что нет действующищ счетов";
            }
            else
            {
                data = "Информация о счетах:";
            }

            return new StringBuilder(data);
        }



    }

    
}